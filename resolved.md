# Resolved issues

* 2022/11 [Formalize reimbursement process to reduce extra paperwork](https://salsa.debian.org/debian/grow-your-ideas/-/issues/1)
  * Launched [Propose the debian-reimbursements project](https://salsa.debian.org/freexian-team/project-funding/-/merge_requests/15)
* 2021/07 [Request HTTPS for forums.debian.net](https://salsa.debian.org/debian/grow-your-ideas/-/issues/3)
  * Let's encrypt is enabled. https://forums.debian.net/
* 2021/06 [Easy, formalized process to apply a VPS instance for an experimental project in Debian](https://salsa.debian.org/debian/grow-your-ideas/-/issues/4)
  * Use https://salsa.debian.org/debian.net-team/requests/-/issues create_vps_instance issue
