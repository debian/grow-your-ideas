# Grow your ideas for Debian Project

This repository is aimed to track incubator project ideas.

There are many "if it could be ..." in Debian Project.

Such a issue should be discussed in appropriate [Debian Mailing List](https://lists.debian.org/completeindex.html).

But you are not confident or not afford to owe executor role of your ideas, you may hesitate to share your ideas on these lists.

In this repository, it is important to lower the barrier.

# How to share your idea 🤔

Please create a [issue](https://salsa.debian.org/debian/grow-your-ideas/-/issues/new??issuable_template=Share%20Your%20Idea) with issue template.

* The problem
* Actual situation
* Expected situation (Goal)
* Additional Information

Above information should be filled at least.

Select "Share Your Idea" in Description to apply issue template.

![Use Issue Teamplate](use_issue_template.png)

# Ready for project funding 💰

If your idea is grown great one and you can get enough members to execute as a project, the issue can be closed and move it forward to [Project Funding](https://salsa.debian.org/freexian-team/project-funding) to make your idea achieved.

# Resolved 🎉

* See [Resolved Issues](resolved.md)

# Reference 📘

This repository is inspired from mailing list discussion.

* [Re: Creating a Debian Spending proposals and discussion mailing list](https://lists.debian.org/debian-project/2021/04/msg00007.html)

