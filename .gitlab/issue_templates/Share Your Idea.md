# The problem

> Describe the problem you found.

# Actual situation

> Describe the issue in detail.
> Why you feels it uncomfortable.

# Expected situation

> Describe the goal of issue. it should be clear to judge.

e.g.

* ... was implemented
* ... was changed to A.

# Additional information

e.g.

* how did you negotiated with stakeholder
* related mailing list discussion
